package com.example.hp.weekone;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;


public class ListingRecyclerView extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private MyAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    String[] myDataset = new String[17];
    String[] cDataset = new String[30];
    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 5;
    String[] temp = new String[2];
    int firstVisibleItem, visibleItemCount, totalItemCount;
    int pastVisiblesItems;
    ArrayList<String> names;
    int counter = 0, i;
    int n, j=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_recycler_view);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        names = new ArrayList<String>(
                Arrays.asList("venkat",
                        "danny",
                        "prithvi",
                        "muthahar",
                        "sachin",
                        "juice", "venkat",
                        "danny",
                        "prithvi",
                        "muthahar",
                        "sachin",
                        "juice"));
        myDataset = new String[]{
                "cake",
                "Venkat",
                "danny",
                "prithvi",
                "muthahar",
                "sachin",
                "juice",
                "cake",
                "venkat",
                "danny",
                "prithvi",
                "muthahar",
                "sachin",
                "juice",
                "cake"


        };
        n = myDataset.length;
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MyAdapter(names);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {

                        //visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mAdapter.getItemCount();
                    pastVisiblesItems = mLayoutManager.findLastCompletelyVisibleItemPosition();
                    if (loading) {
                        loading = false;

                      //  Log.d("condition", "inside");

                        if ((pastVisiblesItems) >= totalItemCount - 1) {

                           if(j<n) {
                               mAdapter.updateDataSet(myDataset[j]);
                               j++;
                           }

                            //mAdapter.notifyItemChanged(totalItemCount+1);
                            //mAdapter.notifyItemRangeChanged(totalItemCount+1, mLayoutManager.getItemCount() );
//                            mAdapter.notifyDataSetChanged();
                        }
                        loading = true;

                    }
                }
            }
        });

    }
}
