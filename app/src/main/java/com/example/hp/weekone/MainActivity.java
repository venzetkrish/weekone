package com.example.hp.weekone;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hp.weektwo.SenseActivity;
import com.example.hp.weektwo.VibrateActivity;

public class MainActivity extends AppCompatActivity {
    DatabaseActivity db;
    EditText emailField, pwdField,cpwdField;
    Button regButton, logButton;
    String emailEntry, pwdEntry, cpwdEntry,emailPattern, emailId;
    TextView loginText,registerText;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new DatabaseActivity(this);
        emailField =(EditText)findViewById(R.id.editText1);
        pwdField =(EditText)findViewById(R.id.editText2);
        cpwdField =(EditText)findViewById(R.id.editText3);
        regButton =(Button)findViewById(R.id.registerButton);
        logButton =(Button)findViewById(R.id.loginButton);
        loginText=(TextView)findViewById(R.id.loginText);
        registerText=(TextView)findViewById(R.id.registerText);
        logButton.setVisibility(View.GONE);
        registerText.setVisibility(View.GONE);
        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailEntry = emailField.getText().toString();
                pwdEntry = pwdField.getText().toString();
                cpwdEntry = cpwdField.getText().toString();

                emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                sp=getSharedPreferences("Login", Context.MODE_PRIVATE);
                SharedPreferences.Editor Ed=sp.edit();
                Ed.putString("id", emailField.getText().toString() );
                Ed.commit();
                emailId = sp.getString("id", "");

                if(emailEntry.equals("")|| pwdEntry.equals("")|| cpwdEntry.equals(""))
            {
                Toast.makeText(getApplicationContext(),"Required Fields Empty",Toast.LENGTH_SHORT).show();
            }
            else{
                if(pwdEntry.equals(cpwdEntry)&& emailEntry.matches(emailPattern)){
                    boolean chkmail=db.chkemail(emailEntry);
                    if(chkmail==true)
                    {
                        boolean ins=db.insert(emailEntry, pwdEntry);
                        if(ins==true)
                        {
                            Toast.makeText(getApplicationContext(),"Registerd Successfully",Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"Aready Registered",Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(), "username or passwords invalid", Toast.LENGTH_SHORT).show();
                }
            }
            }
        });
    }

    public void clearfields(){
        emailField.setText("");
        pwdField.setText("");
        cpwdField.setText("");
    }
    public void textButton1(View view)
    {
      logButton.setVisibility(View.VISIBLE);
      regButton.setVisibility(View.GONE);
      cpwdField.setVisibility(View.GONE);
      loginText.setVisibility(View.GONE);

      registerText.setVisibility(View.VISIBLE);

           clearfields();
           if(emailId!=null)
           {emailField.setText(emailId);}



    }
    public void textButton2(View view)
    {
        logButton.setVisibility(View.GONE);
        regButton.setVisibility(View.VISIBLE);
        cpwdField.setVisibility(View.VISIBLE);
        loginText.setVisibility(View.VISIBLE);
        registerText.setVisibility(View.GONE);

        clearfields();

    }
    public void login(View view)
    {
        emailEntry = emailField.getText().toString();
        pwdEntry = pwdField.getText().toString();
        boolean chklogin=db.chklogin(emailEntry, pwdEntry);
        if(chklogin==true)
        {
            Toast.makeText(getApplicationContext(),"Login Successful",Toast.LENGTH_SHORT).show();
            startActivity(new Intent(MainActivity.this,SenseActivity.class));
        }
        else
        {
            Toast.makeText(getApplicationContext(),"Invalid Username or Password",Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.button:
                startActivity(new Intent(MainActivity.this,ListActivity.class));
                return true;
            case R.id.drop1:
                Toast.makeText(this,"Thank You",Toast.LENGTH_SHORT).show();
                    finish();
                    System.exit(0);
                return true;
            case R.id.fragment:
                startActivity(new Intent(MainActivity.this,FragmentActivity.class));
                return true;
            case R.id.recycler:
                startActivity(new Intent(MainActivity.this,ListingRecyclerView.class));
                default:
                    return true;
        }

    }
}
