package com.example.hp.weekone;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {
    ListView listView;
    public static final int totalItems=30;
    public static final int perPage=6;
    public static final int remainingItems= totalItems % perPage;
    public static final int lastPage=totalItems/perPage;
    String[] values;
    int curPage=0;
    Button nextButton,prevButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        listView = (ListView)findViewById(R.id.list);
        nextButton=(Button)findViewById(R.id.buttonNext);
        prevButton=(Button)findViewById(R.id.buttonPrev);

         values = new String[] { "venkat",
                "danny",
                "prithvi",
                "muthahar",
                "sachin",
                "juice",
                "cake",
                "Venkat",
                "danny",
                "prithvi",
                "muthahar",
                "sachin",
                "juice",
                "cake","venkat",
                "danny",
                "prithvi",
                "muthahar",
                "sachin",
                "juice",
                "cake",
                "Venkat",
                "danny",
                "prithvi",
                "muthahar",
                "sachin",
                "juice",
                "cake","muthahar",
                "sachin"
        };

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int i, long l) {

                Toast.makeText(ListActivity.this,generatePage(curPage).get(i), Toast.LENGTH_SHORT)
                        .show();

            }

        });

        listView.setAdapter(new ArrayAdapter<String>(ListActivity.this,
                android.R.layout.simple_list_item_1, generatePage(curPage)));
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                curPage+=1;
                listView.setAdapter(new ArrayAdapter<String>(ListActivity.this,
                        android.R.layout.simple_list_item_1, generatePage(curPage)));
                buttonAvailability();
            }
        });
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                curPage-=1;
                listView.setAdapter(new ArrayAdapter<String>(ListActivity.this,
                        android.R.layout.simple_list_item_1, generatePage(curPage)));
                buttonAvailability();
            }
        });
         /*
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {


                int itemPosition     = position;
                String  itemValue    = (String) listView.getItemAtPosition(position);
                Toast.makeText(getApplicationContext(),
                        "Position :"+itemPosition+"  Value : " +itemValue , Toast.LENGTH_LONG)
                        .show();

            }

        }); */
    }

    public ArrayList<String> generatePage(int currentPage)
    {
        int startItem=currentPage*perPage;
        int noOfData=perPage;
        ArrayList<String> pageData=new ArrayList<>();
        if(currentPage==lastPage&&remainingItems>0)
        {
            for(int i=startItem;i<startItem+remainingItems;i++)
            {
                pageData.add(values[i]);
            }

        }
        else
        {
            for(int i=startItem;i<startItem+noOfData;i++)
            {
                pageData.add(values[i]);
            }
        }
        return pageData;
    }

    private void buttonAvailability(){
        if(curPage==lastPage){
        prevButton.setEnabled(true);
        nextButton.setEnabled(false);
        }
        else if(curPage==0){
            prevButton.setEnabled(false);
            nextButton.setEnabled(true);
        }
        else{
            prevButton.setEnabled(true);
            nextButton.setEnabled(true);
        }
    }
}
