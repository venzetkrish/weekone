package com.example.hp.weekone;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.*;

public class WelcomeActivity extends AppCompatActivity {
    private ViewPager viewPager;
    private int[] layouts;
    private Button btnSkip,btnNext;
    private MyPageAdapter pagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        if(!isFirstTimeStartApp())
        {

            startMainActivity();
            finish();
        }



        viewPager=(ViewPager)findViewById(R.id.viewPage);
        btnSkip=(Button)findViewById(R.id.skipButton);
        btnNext=(Button)findViewById(R.id.nextButton);

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startMainActivity();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            int currentPage=viewPager.getCurrentItem()+1;
            if(currentPage<layouts.length){
                viewPager.setCurrentItem(currentPage);
            }
            else{
                startMainActivity();
            }
            }
        });
        layouts=new int[]{R.layout.activity_onscreen1,R.layout.activity_onscreen2,R.layout.activity_onscreen3};
        pagerAdapter=new MyPageAdapter(layouts,getApplicationContext());
        viewPager.setAdapter(pagerAdapter);

    }

    private boolean isFirstTimeStartApp(){
        SharedPreferences ref=getApplicationContext().getSharedPreferences("weekone", Context.MODE_PRIVATE);
        return ref.getBoolean("FirstTimeStartFlag",true);
    }

    private void setFirstTimeStartStatus(boolean stt){
        SharedPreferences ref=getApplicationContext().getSharedPreferences("weekone", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=ref.edit();
        editor.putBoolean("FirstTimeStartFlag",stt);
        editor.commit();
    }

    private void startMainActivity(){
        setFirstTimeStartStatus(false);
        startActivity(new Intent(WelcomeActivity.this,SplashScreen.class));
        finish();
    }
}
