package com.example.hp.weekone;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;


public class FragmentActivity extends AppCompatActivity {
    EditText name;
    String s;
    Button buttonSend;
    String passedArg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        buttonSend=(Button)findViewById(R.id.actToFrag);
        name=(EditText)findViewById(R.id.editText);
        if(getIntent()!=null) {
            passedArg = getIntent().getStringExtra("value");
       //     name.setText(passedArg);
           Bundle bundle=new Bundle();
            bundle.putString("message", passedArg);
            TextFragment fragobj=new TextFragment();
           fragobj.setArguments(bundle);

        }
    }
    public String getData()
    {
        return(passedArg);
    }
    public void actFrag(View view){
        TextFragment.newInstance("This isTest");
    }
}
