package com.example.hp.weekthree;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.hp.weekone.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class CameraActivity extends AppCompatActivity {
    private static final int REQUEST_PERMISSION = 123;
    private boolean permissionAccepted = false;
    private String [] permissions = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    Bitmap bitmap;
    ImageButton imageButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        imageButton=(ImageButton)findViewById(R.id.imageButton);
        if(!hasPermissions(this, permissions)) {

            ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSION);
        }

    }
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
    @Override

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == 1) {



                File f = new File(Environment.getExternalStorageDirectory().toString());

                for (File temp : f.listFiles()) {

                    if (temp.getName().equals("temp.jpg")) {

                        f = temp;

                        break;

                    }

                }

                try {



                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();



                    bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),

                            bitmapOptions);



                    imageButton.setImageBitmap(bitmap);



                    String path = android.os.Environment

                            .getExternalStorageDirectory()

                            + File.separator

                            + "Phoenix" + File.separator + "default";

                    f.delete();

                    OutputStream outFile = null;

                    File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");

                    try {

                        outFile = new FileOutputStream(file);

                        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);

                        outFile.flush();

                        outFile.close();

                    } catch (FileNotFoundException e) {

                        e.printStackTrace();

                    } catch (IOException e) {

                        e.printStackTrace();

                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                } catch (Exception e) {

                    e.printStackTrace();

                }

            } else if (requestCode == 2) {



                Uri selectedImage = data.getData();

                String[] filePath = { MediaStore.Images.Media.DATA };

                Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);

                c.moveToFirst();

                int columnIndex = c.getColumnIndex(filePath[0]);

                String picturePath = c.getString(columnIndex);

                c.close();

                bitmap = (BitmapFactory.decodeFile(picturePath));

                Log.i("path of image",picturePath+"");

                imageButton.setImageBitmap(bitmap);

            }

        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_PERMISSION:
                if(permissionAccepted  = grantResults[0] == PackageManager.PERMISSION_GRANTED){break;}
                else{Toast.makeText(getApplicationContext(),"camera not permitted",Toast.LENGTH_SHORT).show();}
                if(permissionAccepted = grantResults[1]==PackageManager.PERMISSION_GRANTED){break;}
                else{Toast.makeText(getApplicationContext(),"storage not permitted",Toast.LENGTH_SHORT).show();}
                if(permissionAccepted = grantResults[2]==PackageManager.PERMISSION_GRANTED){break;}
                else{Toast.makeText(getApplicationContext(),"storage not permitted",Toast.LENGTH_SHORT).show();}


        }
        if (!permissionAccepted ) finish();

    }
public void imgClick(View view){
    final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };



    AlertDialog.Builder builder = new AlertDialog.Builder(this);

    builder.setTitle("Add Photo!");

    builder.setItems(options, new DialogInterface.OnClickListener() {

        @Override

        public void onClick(DialogInterface dialog, int item) {

            if (options[item].equals("Take Photo"))

            {

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");

                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));

                startActivityForResult(intent, 1);

            }

            else if (options[item].equals("Choose from Gallery"))

            {

                Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(intent, 2);

            }

            else if (options[item].equals("Cancel")) {

                dialog.dismiss();

            }

        }

    });

    builder.show();

    }

}

