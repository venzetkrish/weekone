package com.example.hp.weektwo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.example.hp.weekone.CommonUtilities;
import com.example.hp.weekone.R;

import java.io.Serializable;

public class SendActivity extends AppCompatActivity {
    EditText sendText;
    String valueText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send);
        sendText=(EditText)findViewById(R.id.sendText);
    }
//button onClick for parcelebale data passing
public void parallelData(View view){
    valueText=sendText.getText().toString();
    ParcelActivity obj=new ParcelActivity(valueText);
    Intent i=new Intent(SendActivity.this,RecieveActivity.class);
    i.putExtra("object",obj);
    startActivity(i);
}
//obj passing serializable
    public void objectAccess(View view){
        valueText=sendText.getText().toString();
        CommonUtilities obj=new CommonUtilities(valueText);
        Intent i=new Intent(SendActivity.this,RecieveActivity.class);
        i.putExtra("object",obj);
        startActivity(i);
    }
// Simple intent Extra passing
    public void intentData(View view){
        valueText=sendText.getText().toString();
        Intent intent = new Intent(this, RecieveActivity.class);
        intent.putExtra("arg",valueText);
        startActivity(intent);
    }

// Data passing using bundle
    public void bundle(View view){
        valueText=sendText.getText().toString();
        Intent intentBundle=new Intent(SendActivity.this,RecieveActivity.class);
        Bundle bundle=new Bundle();
        bundle.putString("bun",valueText);
        intentBundle.putExtras(bundle);
        startActivity(intentBundle);

    }

}
