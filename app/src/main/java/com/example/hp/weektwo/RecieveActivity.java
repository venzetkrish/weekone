package com.example.hp.weektwo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.hp.weekone.CommonUtilities;
import com.example.hp.weekone.R;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
//remove the required comment to pass the data as per requirement
public class RecieveActivity extends AppCompatActivity{

    TextView recvText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recieve);
        recvText=(TextView)findViewById(R.id.recvText);
//INTENT
          /*    String passedArg = getIntent().getStringExtra("arg");
        recvText.setText(passedArg);
                   Toast.makeText(getApplicationContext(), "INTENT METHOD", Toast.LENGTH_SHORT).show(); */
//BUNDLE
          /*Intent intent=getIntent();
                Bundle bunValues=intent.getExtras();
                String venName=bunValues.getString("bun","not found");
//SERIALIZABLE
            recvText.setText(venName);*/
       /* CommonUtilities object= (CommonUtilities) getIntent().getSerializableExtra("object");
        String s=object.getT();
        recvText.setText(s);*/
//DEFAULT PARCELABLE
       ParcelActivity object=getIntent().getParcelableExtra("object");
       String string=object.getT();
       recvText.setText(string);
    }
    public void backButton(View view)
    {
        startActivity(new Intent(this,SendActivity.class));
    }
}
