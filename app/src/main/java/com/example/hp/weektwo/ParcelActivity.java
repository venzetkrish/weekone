package com.example.hp.weektwo;

import android.os.Parcel;
import android.os.Parcelable;
//parcelable data transmission class
public class ParcelActivity implements Parcelable {
    String textCopy;
    public ParcelActivity(String s){
        this.textCopy=s;
    }
    public String getT(){return textCopy;}
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.textCopy);
    }
    protected ParcelActivity(Parcel in) {
        this.textCopy = in.readString();
        }
        //creator class
    public static final Parcelable.Creator<ParcelActivity> CREATOR = new Parcelable.Creator<ParcelActivity>() {
        @Override
        public ParcelActivity createFromParcel(Parcel source) {
            return new ParcelActivity(source);
        }

        @Override
        public ParcelActivity[] newArray(int size) {
            return new ParcelActivity[size];
        }
    };

}
