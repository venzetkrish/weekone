package com.example.hp.weektwo;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.hp.weekone.R;

public class SenseActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    VibrateFragment vibFrag;
    RecorderFragment recFrag;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sense);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(2);

        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                viewPager.setCurrentItem(position,false);
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        fixFragment(viewPager);
    }
//set fragments to viewpager using adapter function i.e addFragment
    private void fixFragment(ViewPager viewPager)
    {
        ViewAdapter adapter = new ViewAdapter(getSupportFragmentManager());
        Bundle bundle = new Bundle();
        bundle.putString("params", "THIS IS A VIBRATE SCREEN");
        vibFrag=new VibrateFragment();
        vibFrag.setArguments(bundle);
        recFrag=new RecorderFragment();
        adapter.pushFragment(vibFrag,"VIBRATE");
        adapter.pushFragment(recFrag,"RECORD");
        viewPager.setAdapter(adapter);
    }
}
